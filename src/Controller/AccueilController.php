<?php

namespace App\Controller;

use App\Entity\Competence;
use App\Entity\Reseau;
use App\Entity\Texte;
use App\Repository\TexteRepository;
use App\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $texte = $em->getRepository(Texte::class)->findOneBy(['location' => 'accueil']);
        $texte = $texte->getContenu();

        $texteFooter = $em->getRepository(Texte::class)->findOneBy(['location' => 'footer']);
        $texteFooter = $texteFooter->getContenu();

        $projets = $em->getRepository(Projet::class)->findAll();
        $competences = $em->getRepository(Competence::class)->findAll();


        $arrNoms = [];
        $arrLvls = [];


        foreach ($competences as $competence){
            $arrNoms[] = $competence->getNom();
            $arrLvls[] = $competence->getLvl();
        }

        $arrNoms = implode(',',$arrNoms);
        $arrLvls = implode(',',$arrLvls);

        $reseaux = $em->getRepository(Reseau::class)->findAll();

        return $this->render('accueil/index.html.twig', [
            'texte' => $texte,
            'projets' => $projets,
            'arrNoms'=>$arrNoms,
            'arrLvls'=>$arrLvls,
            'reseaux' => $reseaux,
            'texteFooter' => $texteFooter,
        ]);
    }

    /**
     * @Route("/*")
     */
    public function getReseaux()
    {
        $em = $this->getDoctrine()->getManager();
        $reseaux = $em->getRepository(Reseau::class)->findAll();

        $texteFooter = $em->getRepository(Texte::class)->findOneBy(['location' => 'footer']);
        $texteFooter = $texteFooter->getContenu();


        return $this->render('base.html.twig', [
            'reseaux' => $reseaux,
            'texteFooter' => $texteFooter,
        ]);
    }
}
