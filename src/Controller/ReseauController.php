<?php

namespace App\Controller;

use App\Entity\Reseau;
use App\Form\ReseauType;
use App\Repository\ReseauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reseau")
 */
class ReseauController extends AbstractController
{
    /**
     * @Route("/", name="reseau_index", methods={"GET"})
     */
    public function index(ReseauRepository $reseauRepository): Response
    {
        return $this->render('reseau/index.html.twig', [
            'reseaus' => $reseauRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="reseau_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $reseau = new Reseau();
        $form = $this->createForm(ReseauType::class, $reseau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reseau);
            $entityManager->flush();

            return $this->redirectToRoute('reseau_index');
        }

        return $this->render('reseau/new.html.twig', [
            'reseau' => $reseau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reseau_show", methods={"GET"})
     */
    public function show(Reseau $reseau): Response
    {
        return $this->render('reseau/show.html.twig', [
            'reseau' => $reseau,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="reseau_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Reseau $reseau): Response
    {
        $form = $this->createForm(ReseauType::class, $reseau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reseau_index');
        }

        return $this->render('reseau/edit.html.twig', [
            'reseau' => $reseau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reseau_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Reseau $reseau): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reseau->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reseau);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reseau_index');
    }
}
